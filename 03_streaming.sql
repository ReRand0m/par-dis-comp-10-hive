ADD JAR /opt/cloudera/parcels/CDH/lib/hive/lib/hive-contrib.jar;

USE ${env:USER}_test;

SELECT TRANSFORM(ip)
USING 'cut -d . -f 1' AS ip2
FROM Subnets
LIMIT 10;
