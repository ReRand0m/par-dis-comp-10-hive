-- jar для классов hive, необходимых в Map Reduce задачах
ADD JAR /opt/cloudera/parcels/CDH/lib/hive/lib/hive-contrib.jar;

-- указываем, какую БД хотим использовать (задействуем переменную окружения)
USE ${env:USER}_test;

DROP TABLE IF EXISTS Subnets;

-- EXTERNAL, MANAGED, TEMPORARY
CREATE EXTERNAL TABLE Subnets (
    ip STRING,
    mask STRING
)
ROW FORMAT DELIMITED FIELDS TERMINATED BY  '\t'
STORED AS TEXTFILE
LOCATION '/data/subnets/variant1';

